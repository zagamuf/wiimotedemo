@echo off
if /i "%1"=="run" goto run
start cmd.exe /c %0 run %*
goto end

:run
shift
call vsdefs.bat
set BUILD_DIR="..\build\VS%VSVer%"
if not exist %BUILD_DIR% goto end
if /i "%1"=="log" goto log

:con
cmake ^
  --build %BUILD_DIR% ^
  --target ALL_BUILD ^
  --config Release
pause
goto end

:log
cmake ^
  --build %BUILD_DIR% ^
  --target ALL_BUILD ^
  --config Release ^
  >%BUILD_DIR%\log_make
pause

:end
