@echo off
if /i "%1"=="run" goto run
start cmd.exe /c %0 run %*
goto end

:run
shift
call vsdefs.bat
set SRC_DIR=".."
set BUILD_DIR="..\build\VS%VSVer%"
if not exist %BUILD_DIR% mkdir %BUILD_DIR%
if /i "%1"=="log" goto log

:con
cmake ^
  -S%SRC_DIR% ^
  -B%BUILD_DIR% ^
  -DCMAKE_INSTALL_PREFIX=.. ^
  -G"Visual Studio %VSVer%" ^
  -A%VSArch%
pause
goto end

:log
cmake ^
  -S%SRC_DIR% ^
  -B%BUILD_DIR% ^
  -DCMAKE_INSTALL_PREFIX=.. ^
  -G"Visual Studio %VSVer%" ^
  -A%VSArch% ^
  >%BUILD_DIR%\log_build
pause

:end
