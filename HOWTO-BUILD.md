### How-To Build
1. Install the latest Visual Studio 2019 Community edition including Windows 10 SDK. Select the "Desktop Development with C++" workload. Make sure that "MSVC v142 - VS 2019 C++ x64/x86 build tools" and "Windows 10 SDK" components are selected.

2. Install the latest [CMake](https://cmake.org/download/). Make sure that its binaries are accessible through the **PATH** environment variable.

3. Download the latest [OpenCV](https://sourceforge.net/projects/opencvlibrary/files/4.1.2/opencv-4.1.2-vc14_vc15.exe). Unpack OpenCV to the 'external' subdirectory of the project's directory.

4. The 'scripts' subdirectory contains all needed scripts to build the project:
    - use **build.bat** to prepare the build configuration of the project;
    - use **make.bat** to build the project;
    - use **install.bat** to install the built binaries into the 'bin' subdirectory.
